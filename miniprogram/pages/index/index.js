//获取应用实例
const app = getApp()

Page({
  data: {
    showBottomAlterBack:false,
    iconType: [
      'success', 'success_no_circle', 'info', 'warn', 'waiting', 'cancel', 'download', 'search', 'clear'
    ],
    imgUrls: [
      {
        url: './logo.jpg'
      },
        {
        url: './1.jpg'
      }
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    userInfo: {}
  },
  //事件处理函数,跳转页面
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  bottomAlterClose:function(){
    this.setData({ showBottomAlterBack:false})
  },
  getSetps:function(){
    var beginTime = new Date()
    beginTime.setFullYear(2018, 9, 8)
    beginTime.setHours(0)	 //设置 Date 对象中的小时 (0 ~ 23)。
    beginTime.setMinutes(0)	//设置 Date 对象中的分钟 (0 ~ 59)。
    beginTime.setSeconds(0) //设置 Date 对象中的秒 (0 ~ 59)。

    var endTime = new Date()
    endTime.setFullYear(2018, 11, 10)
    endTime.setHours(23)	 //设置 Date 对象中的小时 (0 ~ 23)。
    endTime.setMinutes(59)	//设置 Date 对象中的分钟 (0 ~ 59)。
    endTime.setSeconds(59) //设置 Date 对象中的秒 (0 ~ 59)。

    if (new Date() < beginTime) {
      wx.showToast({
        icon: 'none',
        title: "活动还未开始"
      })
      return
    }

    if (new Date() > endTime) {
      wx.showToast({
        icon: 'none',
        title: "活动已结束"
      })
      return
    }

    wx.showLoading({
      title:"正在获取步数",
      mask:true
    });
   
    var localFunction = app.localFunction;
    localFunction.getWxSteps(this);
  },
  donateSteps:function(e){
    var localFunction = app.localFunction;
    var date = localFunction.getDate();
    var currDateUserSteps = localFunction.queryCurrDateUserSteps()

    var steps = e.currentTarget.dataset.steps;
    console.log(steps)
    if(steps <= 0){
      wx.showToast({
        icon: 'none',
        title: "没有运动信息"
      })
      return
    }

    currDateUserSteps.then(
      function(data){
        console.log("当前用户当天捐赠情况：", data)
        if (data.length >= 2) {
          console.log("没人每天最多捐赠两次")
          wx.showToast({
            icon: 'none',
            title: "每人每天最多捐赠两次"
          })
          return
        }else{
          var tmpSteps = 0;
          if(data.length == 1){
            tmpSteps = steps - data[0].steps
            if(tmpSteps <= 0){
              wx.showToast({
                icon: 'none',
                title: '没有新的运动信息'
              })
              return
            }
          }else{
            tmpSteps = steps 
          }
          console.log("用户:", app.globalData.userInfo.nickName, "捐赠：", tmpSteps)
          var addResult = localFunction.donateSteps(tmpSteps);
          console.log("addResult:",addResult)
          addResult.then(res => {
            wx.showToast({
              title: '捐赠成功',
            })
            getCurrentPages()[getCurrentPages().length - 1].setData({ showBottomAlterBack: false });
            wx.navigateTo({
              url: '../haveDonate/haveDonate'
            })
          }).catch(function(e){
            wx.showToast({
              icon: 'none',
              title: '捐赠失败'
            })
            console.log(e)
          })
        }
      }
    )
    
  },
  showTop: function(){//查看排名
    wx.navigateTo({
      url: '../showTop/showTop'
    })
  },
  /**
  * 生命周期函数--监听页面加载
  */
  // onLoad: function (options) {
  //   var app = getApp()
  //   var localFunction = app.localFunction;
  //   localFunction.queryTopSteps(this);
  // },
  index2: function () {//参与活动
    wx.navigateTo({
      url: '../index2/index2'
    })
  },
  test2: function () {
    console.log(app.globalData.userInfo)
    var localFunction = app.localFunction;
    localFunction.queryTopSteps();
  },
  test3: function () {
    var localFunction = app.localFunction;
    localFunction.queryTopSteps(this);
  },
  test4: function () {
    var localFunction = app.localFunction;
    localFunction.getWxSteps(this);
  },
  onShow: function () {

    
    var localFunction = app.localFunction;
    localFunction.queryCountSteps();
    localFunction.queryTopSteps(this, 2);

  },
  onLoad: function () {
    
    var localFunction = app.localFunction;
    // localFunction.queryCountSteps();

    // localFunction.queryTopSteps(this,2);

    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }

    wx.cloud.callFunction({
      name: 'login',
      complete: res => {
        app.globalData.openId=res.result.openid
      }
    })
  },
  getUserInfo: function (e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  onShareAppMessage: function () {
      //分享
    return {
      title: '丰铭广场为爱奔跑',
      webViewUrl: '../logs/logs',
      imageUrl:'./logo.jpg'
    }
  }
})
