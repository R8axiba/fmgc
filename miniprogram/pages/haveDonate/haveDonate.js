// miniprogram/pages/haveDonate/haveDonate.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tmpPic: "",
    imageHeight: ""
  },
  createPic: function(avatarUrl, todaySteps, todayAmont, countSteps, countAmont) {
    var canvas_width = 304;
    var canvas_height = 540;
    const ctx = wx.createCanvasContext('canvas-id', this);
    ctx.drawImage("bg.jpg", 0, 0, canvas_width, canvas_height);

    wx.downloadFile({
      url: avatarUrl,
      success: res => {
        // 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容

        ctx.save();
        ctx.beginPath(); //开始绘制
        ctx.arc(canvas_width / 2, 260, 50, 0, Math.PI * 2, false);
        ctx.clip();
        ctx.drawImage(res.tempFilePath, canvas_width / 2 - 50, 260 - 50, 100, 100);
        ctx.restore();
        ctx.setFontSize(20)
        ctx.setFillStyle("#ffffff");
        ctx.fillText(todaySteps + "步", canvas_width / 2 - 10, 328);
        ctx.fillText(todayAmont + "元", canvas_width / 2 - 10, 358);
        ctx.fillText(countSteps + "步", canvas_width / 2 + 50, 388);
        ctx.fillText(countAmont + "元", canvas_width / 2 + 50, 418);

        ctx.draw(true, function() {
          wx.canvasToTempFilePath({
            width: canvas_width,
            height: canvas_height,
            destWidth: canvas_width,
            destHeight: canvas_height,
            canvasId: 'canvas-id',
            success(res) {
              console.log(res.tempFilePath);

              getCurrentPages()[getCurrentPages().length - 1].setData({
                tmpPic: res.tempFilePath
              });
            }
          })
        })
      },
      fail: res => {
        console.log(res);
      }
    });
  },
  createPic2: function(avatarUrl, todaySteps, todayAmont, countSteps, countAmont) {
    var canvas_width = 1080;
    var canvas_height = 1920;
    const ctx = wx.createCanvasContext('canvas-id2', this);
    wx.showLoading({
      title: "加载中...",
      mask: true
    })

    ctx.drawImage("bg.jpg", 0, 0, canvas_width, canvas_height);
    wx.getSystemInfo({
      success: function(resWindow) {
        var windowWidth = resWindow.windowWidth;
        var windowHeight = resWindow.windowHeight;

        wx.downloadFile({
          url: avatarUrl,
          success: res => {
            // 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容
            ctx.save();
            ctx.beginPath(); //开始绘制
            ctx.arc(canvas_width / 2, 720, 80, 0, Math.PI * 2, false);
            ctx.clip();
            ctx.drawImage(res.tempFilePath, canvas_width / 2 - 80, 720 - 80, 160, 160);
            ctx.restore();
            ctx.setFontSize(60)
            ctx.setFillStyle("#ffffff");
            var todayStepsStr = "今天捐赠" + todaySteps + "步";
            ctx.fillText("今天捐赠" + todaySteps + "步", canvas_width / 2 - 150 - ((todayStepsStr.length - 5)*15), 930);
            var todayAmontStr = "金额" + todayAmont + "元";
            ctx.fillText("金额" + todayAmont + "元", canvas_width / 2 - 90 - ((todayAmontStr.length - 3) * 15), 1010);
            
            ctx.setFontSize(40);
            ctx.fillText("您的爱心和善行我们铭感于心,", canvas_width / 2 - 260, canvas_height - 775);
            ctx.setFontSize(40);
            ctx.fillText("筹集的善款将全部捐献给希望工程,", canvas_width / 2 - 300, canvas_height - 720);
            ctx.setFontSize(40);
            ctx.fillText("传播正能量，助力捐步行，", canvas_width / 2 - 210, canvas_height - 665);
            ctx.setFontSize(40);
            ctx.fillText("加油笔芯！", canvas_width / 2 - 80, canvas_height - 610);

            ctx.draw(true, function() {
              wx.canvasToTempFilePath({
                width: canvas_width,
                height: canvas_height,
                destWidth: canvas_width,
                destHeight: canvas_height,
                canvasId: 'canvas-id2',
                success(res) {
                  var cale = 1080 / 608; //图片比例
                  getCurrentPages()[getCurrentPages().length - 1].setData({
                    imageHeight: resWindow.windowWidth * cale,
                    createPicUrl: res.tempFilePath,
                    tmpPic: res.tempFilePath
                  });
                }
              })
            })
            //隐藏加载数据
            wx.hideLoading();
          },
          fail: res => {
            console.log(res);
          }
        });
      }
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var app = getApp()
    var localFunction = app.localFunction;
    var date = localFunction.getDate();
    var currDateUserSteps = localFunction.queryCurrDateUserSteps()
    var countStepsQuery = localFunction.countSteps()

    currDateUserSteps.then(
      function(data) {
        var todaySteps = 0;
        for (var i = 0; i < (data.length <= 2 ? data.length : 2); i++) {
          todaySteps = todaySteps + data[i].steps
        }

        countStepsQuery.then(function(res) {
          var countSteps = 0;
          var avatarUrl = res.data[0].avatarUrl;
          var nickName = res.data[0].nickName;
          for (var i = 0; i < res.data.length; i++) {
            countSteps = countSteps + res.data[i].steps;
          }

          var todayAmont = (todaySteps / 6666).toFixed(2)
          var countAmont = (countSteps / 6666).toFixed(2)
          getCurrentPages()[getCurrentPages().length - 1].setData({
            todaySteps: todaySteps,
            countSteps: countSteps,
            todayAmont: todayAmont,
            countAmont: countAmont
          });
          getCurrentPages()[getCurrentPages().length - 1].createPic2(avatarUrl, todaySteps, todayAmont, countSteps, countAmont)
        })
      }
    )
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
  savePic: function() {
    wx.authorize({
      scope: "scope.writePhotosAlbum",
      success: function() {
        wx.saveImageToPhotosAlbum({
          filePath: getCurrentPages()[getCurrentPages().length - 1].data.tmpPic,
          success: function() {
            wx.showToast({
              title: '保存图片成功',
            })
          }
        })
      }
    });

  }
})