//获取应用实例
const app = getApp()

Page({
  data: {
    showBottomAlterBack:false,
    iconType: [
      'success', 'success_no_circle', 'info', 'warn', 'waiting', 'cancel', 'download', 'search', 'clear'
    ],
    imgUrls: [
      {
        url: '../index/3.jpg'
      },
      {
        url: '../index/2.jpg'
      },
      {
        url: 'https://s1.ax1x.com/2018/10/24/irbuSs.jpg'
      },
      {
        url: 'https://s1.ax1x.com/2018/10/24/irbKln.jpg'
      },
      {
        url: 'https://s1.ax1x.com/2018/10/24/irbJkF.jpg'
      }
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    userInfo: {},
    voteName: '',
    votePhone: '',
    showUserInfo:'hide',
    isFolded:true
  },
  change: function (e) {
    this.setData({
      isFolded: !this.data.isFolded,
    })
  },
  //手机号
  votePhone: function (e) {
    var loginPhone = e.detail.value;
      if (!(/^1[3456789]\d{9}$/.test(loginPhone))) {
        this.setData({
          ajxtrue: false
        })
        if (loginPhone.length >= 11) {
          wx.showToast({
            title: '请输入正确的手机号',
            icon: 'none',
            duration: 2000
          })
        }
      }else{
        this.setData({
          votePhone: e.detail.value
        })
      }
  },
  //姓名
  voteName: function (e) {
    this.setData({
      voteName: e.detail.value
    })
  },
  bottomAlterClose:function(){
    this.setData({ showBottomAlterBack:false})
  },
  getSetps:function(){
    var beginTime = new Date()
    beginTime.setFullYear(2018, 9, 8)
    beginTime.setHours(0)	 //设置 Date 对象中的小时 (0 ~ 23)。
    beginTime.setMinutes(0)	//设置 Date 对象中的分钟 (0 ~ 59)。
    beginTime.setSeconds(0) //设置 Date 对象中的秒 (0 ~ 59)。

    var endTime = new Date()
    endTime.setFullYear(2019, 11, 10)
    endTime.setHours(23)	 //设置 Date 对象中的小时 (0 ~ 23)。
    endTime.setMinutes(59)	//设置 Date 对象中的分钟 (0 ~ 59)。
    endTime.setSeconds(59) //设置 Date 对象中的秒 (0 ~ 59)。

    //姓名
    var voteName = this.data.voteName;
    //手机号
    var votePhone = this.data.votePhone;

    //判断是否填写信息-姓名
    if (voteName == "") {
      //隐藏加载数据
      wx.hideLoading();

      //提示信息
      wx.showToast({
        title: '请填写您的姓名',
        icon: 'none',
        duration: 2000
      });
      return false;
    }

    //判断是否填写信息-手机号
    if (!(/^1[3456789]\d{9}$/.test(votePhone))) {
      //提示信息
      wx.showToast({
        title: '请输入正确的手机号',
        icon: 'none',
        duration: 2000
      });
      return false;
    }

    if (new Date() < beginTime) {
      wx.showToast({
        icon: 'none',
        title: "活动还未开始"
      })
      return
    }

    if (new Date() > endTime) {
      wx.showToast({
        icon: 'none',
        title: "活动已结束"
      })
      return
    }

    wx.showLoading({
      title:"正在获取步数",
      mask:true
    });
   
    var localFunction = app.localFunction;
    localFunction.getWxSteps(this);
  },
  donateSteps:function(e){
    var localFunction = app.localFunction;
    var date = localFunction.getDate();
    var currDateUserSteps = localFunction.queryCurrDateUserSteps()

    var steps = e.currentTarget.dataset.steps;
    var name = e.currentTarget.dataset.name;
    var phone = e.currentTarget.dataset.phone;
    console.log(steps)
    if(steps <= 0){
      wx.showToast({
        icon: 'none',
        title: "没有运动信息"
      })
      return
    }

    currDateUserSteps.then(
      function(data){
        console.log("当前用户当天捐赠情况：", data)
        if (data.length >= 2) {
          console.log("没人每天最多捐赠两次")
          wx.showToast({
            icon: 'none',
            title: "每人每天最多捐赠两次"
          })
          return
        }else{
          var tmpSteps = 0;
          if(data.length == 1){
            tmpSteps = steps - data[0].steps
            if(tmpSteps <= 0){
              wx.showToast({
                icon: 'none',
                title: '没有新的运动信息'
              })
              return
            }
          }else{
            tmpSteps = steps 
          }
          console.log("用户:", app.globalData.userInfo.nickName, "捐赠：", tmpSteps)
          var addResult = localFunction.donateSteps(tmpSteps,name,phone);
          console.log("addResult:",addResult)
          addResult.then(res => {
            localFunction.updateCount(tmpSteps, name, phone)
            wx.showToast({
              title: '捐赠成功',
            })
            getCurrentPages()[getCurrentPages().length - 1].setData({ showBottomAlterBack: false });
            wx.navigateTo({
              url: '../haveDonate/haveDonate'
            })
          }).catch(function(e){
            wx.showToast({
              icon: 'none',
              title: '捐赠失败请重试'
            })
            console.log(e)
          })
        }
      }
    ).catch(ee=>{
      wx.showToast({
        icon: 'none',
        title: '获取运行信息失败，请重试'
      })
      console.log(e)
    });
    
  },
  showTop: function(){
    wx.navigateTo({
      url: '../showTop/showTop'
    })
  },
  test2: function () {
    console.log(app.globalData.userInfo)
    var localFunction = app.localFunction;
    localFunction.queryTopSteps();
  },
  test3: function () {
    var localFunction = app.localFunction;
    localFunction.queryTopSteps(this);
  },
  test4: function () {
    var localFunction = app.localFunction;
    localFunction.getWxSteps(this);
  },
  onShow: function () {
    var localFunction = app.localFunction;
    localFunction.queryCountSteps();
  },
  onLoad: function () {
    
    var localFunction = app.localFunction;
    localFunction.queryCountSteps();
    
    var phoneName = localFunction.queryPhoneName();
    phoneName.then(function(data){
      // this.data.votePhone = phoneName.phone;
      // this.data.votePhone = phoneName.phone;
      // this.data.voteName = phoneName.name;
      // this.data.voteName = phoneName.name;
      if (data && data[0] && data.length >=1){
        getCurrentPages()[getCurrentPages().length - 1].setData({
          votePhone: data[0].phone,
          voteName: data[0].name,
          showUserInfo:"hide"
        });
      }else{
        showUserInfo: ""
        getCurrentPages()[getCurrentPages().length - 1].setData({
          showUserInfo: ""
        });
      }
      
    });
    

    console.log("--------", phoneName.phone);

    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }

    wx.cloud.callFunction({
      name: 'login',
      complete: res => {
        app.globalData.openId=res.result.openid
      }
    })
  },
  getUserInfo: function (e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
