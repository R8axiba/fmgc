// miniprogram/pages/login/login.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    autoLogin()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindGetUserInfo:function(){
    wx.redirectTo({
      url: "../index/index"
    })
  }

})

function autoLogin() {

  console.log("自动登录")
  console.log("用户信息", app.globalData.userInfo);

  wx.login({
    success: function (res) {
      console.log("登录成功")
      console.log("code:"+res.code)

      console.log("用户信息", app.globalData.userInfo);
      wx.getSetting({
        success: function (authSetting) {
          console.log(authSetting);
          var setting = authSetting.authSetting;
          if (setting["scope.userInfo"] != true) {
            console.log("没有授权");
            wx.showToast({
              title: '没有同意授权',
              icon: 'none',
              duration: 2000
            });

          } else {
            console.log("已经授权");
            wx.redirectTo({
              url: "../index/index"
            })
          }
        },
        fail: function () {
          console.log("获取授权字符串失败")
          wx.showToast({
            title: '授权失败，请重试',
            icon: 'none',
            duration: 2000
          });
        },
        complete: function () {

        }
      })

    },
    fail: function () {
      wx.showToast({
        title: '登录失败，请重试',
        icon: 'none',
        duration: 2000
      });
    },
    complete: function () {

    }
  })
  
}